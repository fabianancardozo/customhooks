import { ChangeEvent } from 'react';
import { useState } from 'react';
import { useForm } from '../hooks/useForm';


export const Form2 = () => {

    const {form, handleChange} = useForm({
        postal:'ABC123',
        address: 'Street New York 123'
    })
    
    const{postal, address} = form;

  return (  
    <form autoComplete='off'>
        <div className='mb-3'>
            <label className='form-label'>Fiscal Code:</label>
            <input
                type='text'
                className='form-control'
                name='postal'
                value={postal}
                onChange={handleChange}
            />
        </div>

        <div className='mb-3'>
            <label className='form-label'>Eddress:</label>
            <input
                type='text'
                className='form-control'
                name='address'
                value={address}
                onChange={handleChange}
            />
        </div>
        <pre>{JSON.stringify(form)}</pre>
    </form>
    );



};
