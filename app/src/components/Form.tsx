import { ChangeEvent } from 'react';
import { useState } from 'react';
import { useForm } from '../hooks/useForm';

interface FormData {
    email: string,
    name: string,
    age: number,
}


export const Form = () => {

    const {form, handleChange} = useForm <FormData> ({
        email:'fornando@ejemplo',
        name:'Fernando',
        age:35,
    })
    //desestructuro/llamo los valores de form para utilizarlo posteriormente en input
    const {email, name, age} = form;

    
  return ( 
    <form autoComplete='off'>
        <div className='mb-3'>
            <label className='form-label'>Email:</label>
            <input
                type='email'
                className='form-control'
                name='email'
                /* al email lo obtengo de la const [form] */
                value={email}
                onChange={handleChange}
            />
        </div>

        <div className='mb-3'>
            <label className='form-label'>Name:</label>
            <input
                type='text'
                className='form-control'
                name='name'
                value={name}
                onChange={handleChange}
            />
        </div>

        <div className='mb-3'>
            <label className='form-label'>Age:</label>
            <input
                type='number'
                className='form-control'
                name='age'
                value={name}
                onChange={handleChange}
            />
        </div>

        {/* con esto imprimo en pantalla el valor de mi form */}
        <pre>{JSON.stringify(form)}</pre>
    </form>
    );
};



