//siempre los customHooks llevan la palabra use adelante

import { ChangeEvent, useState } from "react";


//use Form es de tipo T generico y el initialState es del T que sea que me mande a la hora de llamar el useForm con esta T manejo la interfaz FormData
//export function useForm<T>(initState:T){
export const useForm = <T extends Object> (initState: T) => {
    
    //para mantener el estado del formulario
    const [form, setForm] = useState(initState);

    const handleChange = ({target}: ChangeEvent<HTMLInputElement>) => {
        const {name, value} = target;
        setForm({
            ...form,
            [name]: value,
        })
    }
    return{
        form,
        handleChange,
        /* regreso todo lo que se encuentre en mi form de forma desestructurada */
        ...form,
    }
};

//el handleChange, es una forma universal de llamar a la const que se llama cuando cambia el input, 
//target viene desestructurado de e
//lo que hace este evento target es llamar a onChange
//onChange recibe un evente ChangeEvent que es una interfaz predefinida de React
//esta interfaz es generica, por ende debemos especificar que es lo que esta cambiando este evento

//desestructo/llamo a name y value desde target : const {name, value} = target;
//actualizo el state: setForm({
//desestructuro/llamo al valor actual del form: ...form,
//cambio la propiedad de name: [name]: value,
