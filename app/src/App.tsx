import './App.css';
import { Form } from './components/Form';
import { Form2 } from './components/Form2';

function App() {
  return (
    <div className="App">
      <header className="App-header">
      <h1>CustomHooks</h1>
      <hr />
      <Form/>
      <hr />
      <Form2/>
      </header>
    </div>
  );
}

export default App;
